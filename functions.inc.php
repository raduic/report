<?php
// functions

function makeRecursiveDir($path) {
    $path = str_replace("\\", "/", $path);
    $path = explode("/", $path);
    $maxDept = 5; // must implement the max directory path
    $rebuild = '';

    foreach($path as $p) {      
        if(strstr($p, ":") != false) {
            echo "\nFound : in $p\n";
            $rebuild = $p;
            continue;
        }
        $rebuild .= "$p/";
        // echo "Checking: $rebuild \n";
        if(!is_dir($rebuild)) {
            mkdir($rebuild, 0777);
            chmod($rebuild, 0777);
        }
    }
}

    /*
    * get files and folders from inside a link / directory 
    */
function getDirContents($dir, &$results = array()) {
    $files = scandir($dir);

    foreach ($files as $key => $value) {
        $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
        if (!is_dir($path)) {
            $results[] = $path;
        } elseif ($value != "." && $value != "..") {
            getDirContents($path, $results);
            $results[] = $path;
        }
    }
    return $results;
}

    /* 
    *   writing a log file in specific point of code
    */
    function writeLog($log_msg) {
   
    }

    /*
    *   return variable values of array for debbuging
    */
    function returnValue($data) {
        foreach ($data as $key => $value) {
            echo ($key . " => " . $value);
            echo '<br />';
        }    
    }
    