<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  
    <body>
    <div class="jumbotron vertical-center">
        <div class="container">
        <form action="" method="POST" enctype="multipart/form-data">
            <input class="form-control form-control-lg"  id="loaded_file" type="file" name="file">
            <div class="col-auto">
                <button type="submit" name="submit" class="btn btn-primary mb-3">Upload file!</button>
            </div>
        </form>
        </div>
    </div>
  </body>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->



</html>

<?php
include 'functions.inc.php';

$uploads = "uploads";
// mkdir($uploads);

$sizeKb = 10485760;

if (isset($_POST['submit'])) {
    $file = $_FILES['file'];

    $fileName = $_FILES['file']['name'];
    $fileType = $_FILES['file']['type'];
    $fileTmpName = $_FILES['file']['tmp_name'];
    $fileError = $_FILES['file']['error'];
    $fileSize = $_FILES['file']['size'];

    $fileExt = explode('.' , $fileName );

    $fileActualExt = strtolower(end($fileExt));

    $allowed = array('csv', 'xls');

    if (in_array($fileActualExt, $allowed)) {
        if ($fileError === 0) {
            if ($fileSize <= $sizeKb ) {
                $fileNameNew = uniqid('', true).".".$fileActualExt;
                $fileDestination = 'uploads/'.$fileNameNew;
                move_uploaded_file($fileTmpName, $fileDestination);
               // header("Location: ceva.php");

               $row = 1;
               if (($handle = fopen($fileDestination, "r")) !== FALSE) {
                    while (($data = fgetcsv($handle, 500, ";")) !== FALSE) {
                        $num = count($data);
                        $pwd = getcwd();
                        $path = [];
                        array_push($path, $uploads, $data[3], $data[8], $data[0]);
                        $dir = join(DIRECTORY_SEPARATOR, $path);

                        makeRecursiveDir($dir);
                         $row++;
                    }
                fclose($handle);
               } 
            }
        }
    }
}
